provider "aws" {
    region = "ap-south-1"
    access_key = "AKIA6FG6EN5KDVCRLXPG"
    secret_key = "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"
} 

variable "cidr_blocks" {
    description = "cidr blocks names for vpc and subnet"
    type = list (object({
        cidr_block = string
        name = string
    }))
}

resource "aws_vpc" "development_vpc" {
    cidr_block = var.cidr_blocks[0].cidr_block
    tags = {
        Name: var.cidr_blocks[0].name
    }
}

resource "aws_subnet" "dev-subnet-1" {
    vpc_id = aws_vpc.development_vpc.id
    cidr_block = var.cidr_blocks[1].cidr_block
    availability_zone = "ap-south-1a"
    tags = {
        Name: var.cidr_blocks[1].name
    }
}

data "aws_vpc" "existing_vpc" {
    default = true
}

output "dev-vpc-id" {
    value = aws_vpc.development_vpc.id
}

output "dev-subnet-id" {
    value = aws_subnet.dev-subnet-1.id
}